from csv import DictReader, DictWriter
import json

csv_data = 'employees_data.csv'

class employees_data_model:
    def __init__(self, filename) -> None:
        self.filename = filename

    def find_by_name(name: str): 
        with open(csv_data) as file:
            reader = DictReader(file)
            employees_data = list(reader)
            employee_data = [x for x in employees_data if name.title() in x.get('Nome')]
            return json.dumps(employee_data)

    def find_by_cpf(cpf: str):
        with open(csv_data) as file:
            reader = DictReader(file)
            employees_data = list(reader)
            employee_data = [x for x in employees_data if cpf.title() in x.get('Cpf')]
            return json.dumps(employee_data)

    def find_by_role(role: str):
        with open(csv_data) as file:
            reader = DictReader(file)
            employees_data = list(reader)
            employee_data = [x for x in employees_data if role.title() in x.get('Cargo')]
            return json.dumps(employee_data)

    def find_by_date_of_register(date: str):
        with open(csv_data) as file:
            reader = DictReader(file)
            employees_data = list(reader)
            employee_data = [x for x in employees_data if date in x.get('DataCad')]
            return json.dumps(employee_data)

    def find_by_uf(uf: str):
        with open(csv_data) as file:
            reader = DictReader(file)
            employees_data = list(reader)
            employee_data = [x for x in employees_data if uf.upper() in x.get('UfNasc')]
            return json.dumps(employee_data)

    def find_by_salary(salary: str):
        with open(csv_data) as file:
            reader = DictReader(file)
            employees_data = list(reader)
            employee_data = [x for x in employees_data if salary in x.get('Salario')]
            return json.dumps(employee_data)

    def find_by_status(status: str):
        with open(csv_data) as file:
            reader = DictReader(file)
            employees_data = list(reader)
            employee_data = [x for x in employees_data if status.upper() in x.get('Status')]
            return json.dumps(employee_data)

    def add_or_update_employee(body):
        response = json.loads(body)
        with open(csv_data) as file:
            reader = DictReader(file)
            employees_data = list(reader)
            repeated_data = [x for x in employees_data if response.get('Cpf') == x.get('Cpf')]
            if len(repeated_data) > 0:
                with open(csv_data, 'w') as wf:
                    header = response.keys()
                    writer = DictWriter(wf, fieldnames=header)
                    employees_data.remove(repeated_data[0])
                    employees_data.append(response)
                    writer.writeheader()
                    writer.writerows(employees_data)
                    return {"message": "User updated"}
        with open(csv_data, 'a', newline='\n') as file:
            writer = DictWriter(file, fieldnames=response.keys())
            writer.writerow(response)
            return {"message": "User added!"}

    def delete_employee_by_cpf(cpf: str):
        response = ''
        with open(csv_data) as file:
            reader = DictReader(file)
            employees_data = list(reader)
            new_file = [x for x in employees_data if cpf != x.get('Cpf')]
            if len(new_file) == len(employees_data):
                return {"message": "User not found!"}
            response = new_file
        with open(csv_data, 'w') as file:
            header = list(response[0].keys())
            writer = DictWriter(file, fieldnames=header)
            writer.writeheader()
            writer.writerows(response)
            return {"message": "User deleted!"}
