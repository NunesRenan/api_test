from flask import Flask


def init_app(app:Flask):
    from .employees_data_view import bp
    app.register_blueprint(bp)