import json
from flask import Blueprint, request
from app.models.employees_data_model import employees_data_model


bp = Blueprint('/data', __name__)


@bp.route("/data/filter_by_name/<name>", methods=["GET"])
def filter_by_name_view(name=''):
    return employees_data_model.find_by_name(name)

@bp.route("/data/filter_by_cpf/<cpf>", methods=["GET"])
def filter_by_cpf_view(cpf=''):
    return employees_data_model.find_by_cpf(cpf)

@bp.route("/data/filter_by_role/<role>", methods=["GET"])
def filter_by_role_view(role=''):
    return employees_data_model.find_by_role(role)

@bp.route("/data/filter_by_reg_date/<date_of_register>", methods=["GET"])
def filter_by_date_of_refister_view(date_of_register=''):
    return employees_data_model.find_by_date_of_register(date_of_register)

@bp.route("/data/filter_by_uf/<uf>", methods=["GET"])
def filter_by_uf(uf=''):
    return employees_data_model.find_by_uf(uf)

@bp.route("/data/filter_by_salary/<salary>", methods=["GET"])
def filter_by_salary(salary=''):
    return employees_data_model.find_by_salary(salary)

@bp.route("/data/filter_by_status/<status>", methods=["GET"])
def filter_by_status(status=''):
    return employees_data_model.find_by_status(status)

@bp.route("/data/add_employee", methods=["POST"])
def add_new_employee():
    body = request.get_data()
    return employees_data_model.add_or_update_employee(body)

@bp.route("/data/remove_employee/<cpf>", methods=["DELETE"])
def remove_employee(cpf=''):
    return employees_data_model.delete_employee_by_cpf(cpf)
